import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  zero: boolean;
  twentyfive:boolean;
  fifty:boolean;
  seventyfive:boolean;
  onehundred:boolean;
  constructor() {
    this.zero = false;
    this.twentyfive = false;
    this.fifty = false;
    this.seventyfive = false;
    this.onehundred = false;
  }

  ngOnInit() {
  }

  zeroPercentOver(){
    this.zero = true;
  }
  zeroPercentOut(){
    this.zero = false;
  }

  twentyfivePercentOver(){
    this.zero = true;
    this.twentyfive = true;
  }
  twentyfivePercentOut(){
    this.zero = false;
    this.twentyfive = false;
  }
  fiftyPercentOver(){
    this.zero = true;
    this.twentyfive = true;
    this.fifty = true;
  }
  fiftyPercentOut(){
    this.zero = false;
    this.twentyfive = false;
    this.fifty = false;
  }
  seventyfivePercentOver(){
    this.zero = true;
    this.twentyfive = true;
    this.fifty = true;
    this.seventyfive = true;
  }
  seventyfivePercentOut(){
    this.zero = false;
    this.twentyfive = false;
    this.fifty = false;
    this.seventyfive = false;
  }
  onehundredPercentOver(){
    this.zero = true;
    this.twentyfive = true;
    this.fifty = true;
    this.seventyfive = true;
    this.onehundred = true;
  }
  onehundredPercentOut(){
    this.zero = false;
    this.twentyfive = false;
    this.fifty = false;
    this.seventyfive = false;
    this.onehundred = false;
  }

}
